#!/usr/bin/env python3

from argparse import ArgumentParser
import configparser
import os

def parse_config(profile):
    config = configparser.ConfigParser()
    config.read(f'{os.environ["HOME"]}/.aws/credentials')
    
    try:
        print(f" export AWS_ACCESS_KEY_ID={config[profile]['aws_access_key_id']}")
        print(f" export AWS_SECRET_ACCESS_KEY={config[profile]['aws_secret_access_key']}")
        print(f" export AWS_SESSION_TOKEN={config[profile]['aws_session_token']}")
        print(f" export AWS_REGION=ap-southeast-2")
    except KeyError as e:
        print(f"{type(e)}: {e}")

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-p", "--profile", dest="profile",
                        help="Profile to set up", metavar="PROFILE")

    args = parser.parse_args()
    try:
        if args.profile == None:
            raise KeyError('Profile can\'t be empty')
        parse_config(args.profile)
    except KeyError as e:
        print(f"{type(e)}: {e}")
